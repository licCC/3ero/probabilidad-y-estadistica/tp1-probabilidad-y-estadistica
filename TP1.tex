\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{hyperref}

\usepackage{float}
\graphicspath{ {./Graficos/} }
\captionsetup{justification=centering,margin=2cm}

\usepackage[
top    = 1cm,
bottom = 1.5cm,
left   = 1.5cm,
right  = 1.5cm]
{geometry}

\title{Análisis del estado del arbolado urbano público. Provincia de Buenos Aires, año 2011.}
\author{Delfina Martín - Valentina Prato}
\date{March 2020}

\begin{document}

\maketitle

\null\vspace{\fill}
\begin{abstract}
	En este informe se analiza la distribución y las características principales del arbolado urbano público de la provincia de Buenos Aires, Argentina a partir de un análisis exhaustivo de los datos provistos por el Censo Forestal Urbano Publico realizado en el año 2011 en dos comunas al sur de la provincia de Buenos Aires.
\end{abstract}

\newpage

\section{Motivación del problema}
Existe sobrada evidencia \cite{Polucion} de que hay una relación estrecha entre la contaminación del aire y la probabilidad de desarrollar enfermedades de tipo respiratorias. De hecho, en países no desarrollados se estima que la tercera parte de las muertes están directamente relacionadas a causas ambientales.

Los árboles en general y el arbolado urbano público en particular cumplen un papel relevante en la lucha contra la contaminación del aire. El proceso transformador por el cual los árboles convierten gases tóxicos como lo es el CO2 en oxígeno, es el motor de la reducción del calentamiento global y de la emisión de gases de efecto invernadero \cite{KyotoProtocol}.

Es por esta razón que en este informe se pretende analizar el arbolado público urbano en base a la información provista por el Censo Forestal Urbano Público realizado en dos comunas del sur de Buenos Aires en el año 2011.

\section{Análisis de los datos provistos por el Censo Forestal Urbano Público, 2011}
\subsection{Descripción de las variables analizadas}
Se describen a continuación las variables registradas en el Censo Forestal Urbano Público a partir de las cuales se genera el análisis de datos.
\vspace{0.5cm}

\begin{tabular}{|c|l|}
	\hline
	\textbf{Identificador de la variable}&\textbf{Descripción de la variable}\\
	\hline
	Altura&Representa la altura del árbol medida en metros(m) truncada a la unidad\\
	\hline
	Diámetro&Representa el diámetro del árbol medido en centímetros(cm)\\
	\hline
	Inclinación&Representa el ángulo que forma el tronco del árbol respecto a una recta\\
	&perpendicular al suelo medido en grados($^\circ$)\\
	\hline
	Especie&Representa la especie a la que pertenece el árbol*\\
	\hline
	Origen&Representa la procedencia de la especie, dentro de las cuales se encuentran:\\
	&Nativo/Autóctono, Exótico y No determinado\\
	\hline
	Brotes&Representa el número de brotes jóvenes crecidos durante el último año\\
	\hline
\end{tabular}

\vspace*{4mm}
(*)Las especies incluídas en el Censo Forestal Urbano Público se pueden clasificar en las siguientes categorías: Eucalipto, Jacarandá, Palo borracho, Casuarina, Fresno, Ceibo, Ficus, Álamo y Acacia.

\subsection{Desarrollo del análisis de los datos}
\subsection{Análisis preliminar}
En esta sección se presentan diferentes resúmenes de datos generales mediante recursos relativamente sencillos. Este análisis resulta útil para tener un panorama general de los datos con los que se cuenta.

\subsubsection{Altura de los árboles}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.5]{1-BastonesAltura.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.3]{1-TablaAltura.png}
	\end{minipage}
	\caption{Gráfico de barra correspondiente a la altura de los árboles en la población estudiada con su tabla de frecuencias asociada.}
	\label{fig:Altura}
\end{figure}

Como se puede ver en la figura \ref{fig:Altura} y más detelladamente en la tabla de frecuencias asociada a su derecha, la altura de los árboles se concentra entre los 10m y los 16m aproximadamente.

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{2-CajaAltura.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{3-CajaAlturaOrigen.png}
	\end{minipage}
	\caption{Distribución de las alturas de los árboles y distribución de las alturas discriminada por origen.}
	\label{fig:CajaAltura}
\end{figure}

En la figura \ref{fig:CajaAltura} se aprecia con más detalle en qué rangos se concentran los datos del Censo Forestal Urbano Público. La primera cuarta parte de las alturas de los árboles ordenadas oscila entre los valores 1m y 9m mientras que la última cuarta parte varía entre 18m y 35m aproximadamente.\\
En el gráfico izquierdo de la figura \ref{fig:CajaAltura} están representadas la media aritmética y la mediana: 14.02m y 13m respectivamente. Por otra parte, en el gráfico de la derecha de la figura \ref{fig:CajaAltura} se puede observar que tanto la altura promedio como la mayor concentración de valores de los árboles de especies de origen exótico son mayores que estos mismos parámetros pero referidos a los árboles de especies de origen autóctono.


\subsubsection{Diámetro de los árboles}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.5]{4-BastonesDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.3]{4-TablaDiametro}
	\end{minipage}
	\caption{Gráfico de barra correspondiente a los diámetros de los árboles en la población estudiada con su tabla de frecuencias asociada.}
	\label{fig:Diametro}
\end{figure}

En la figura \ref{fig:Diametro} se puede observar que hay una gran cantidad de árboles con diámetros que varían entre 3cm y 63cm. De cualquier forma, es posible ver estos datos con más detalle en la figura \ref{fig:CajaDiametro}. El primer y tercer cuartil están en los valores 24cm y 53cm respectivamente, lo cual deja en evidencia muchos valores extremos. Además el diámetro de los árboles exóticos no varía mucho con respecto al diámetro de los árboles autóctonos, como se puede observar en el segundo gráfico de la figura \ref{fig:CajaDiametro}.

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{5-CajaDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{6-CajaDiametroOrigen.png}
	\end{minipage}
	\caption{Distribución de los diámetros de los árboles y distribución de los diámetros discriminados por origen.}
	\label{fig:CajaDiametro}
\end{figure}

\subsubsection{Inclinación de los árboles}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.5]{7-BastonesInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.4]{7-TablaInclinacion.png}
	\end{minipage}
	\caption{Gráfico de barra correspondiente a las inclinaciones de los árboles en la población estudiada con su tabla de frecuencias asociada.}
	\label{fig:Inclinacion}
\end{figure}

En la figura \ref{fig:Inclinacion} se puede ver como gran parte de los árboles en la población están inclinados entre 0$^{\circ}$ y 5$^{\circ}$. Por otra parte, en la figura \ref{fig:CajaInclinacion} se ve que el 75\% de los árboles analizados tienen inclinación menor o igual a 4$^{\circ}$ lo que se refleja en la mediana de 0$^{\circ}$. La existencia de valores extremos es lo que da lugar al promedio de inclinación de 3.12$^{\circ}$.

Al hacer un análisis en el cuál se diferencia el origen del árbol, como lo es el resumen del gráfico de la derecha de la figura \ref{fig:CajaInclinacion} se puede concluir que los árboles exóticos no se inclinan, salvo casos extremos.

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{8-CajaInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{9-CajaInclinacionOrigen.png}
	\end{minipage}
	\caption{Distribución de las inclinaciones de los árboles y distribución de las inclinaciones discriminadas por origen.}
	\label{fig:CajaInclinacion}
\end{figure}

\subsubsection{Especies de los árboles}

En la figura \ref{fig:Especies} se puede ver que la especie predominante en la población es el Palo Borracho, seguida por la especie Eucalipto dejando en último lugar a la especie Ficus.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{10-BarrasEspecie.png}
	\caption{Árboles por especie.}
	\label{fig:Especies}
\end{figure}

\subsubsection{Origen de los árboles}

Es importante destacar que en la sección anterior se hizo un análisis por especie que no contemplaba el origen. En la figura \ref{fig:Origen} se pueden ver las proporciones de árboles de origen exótico y autóctono, con lo cual la figura \ref{fig:Origen} complementa la figura \ref{fig:Especies}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{11-BarrasOrigen.png}
	\caption{Árboles por origen.}
	\label{fig:Origen}
\end{figure}

\subsubsection{Brotes de los árboles}

En el gráfico \ref{fig:Brotes} se muestra que en promedio cada árbol de la población permitió el crecimiento de 3.737 brotes. Como este valor no se ve afectado por valores extremos o remotos, el promedio resulta una medida de resumen significativa. El primer cuartil, el tercer cuartil y la mediana se encuentran en 3 brotes, 5 brotes y 4 brotes respectivamente.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{12-CajaBrotes.png}
	\caption{Cantidad de brotes por árbol.}
	\label{fig:Brotes}
\end{figure}

El análisis anterior no fue discriminado por especie ni por origen. A continuación se presentan algunos resúmenes gráficos en los que se categoriza la información según especie u origen lo cual permite realizar un análisis más exhaustivo con los datos disponibles.

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.45\linewidth}
		\includegraphics[width=\linewidth]{13-BarrasBrotesEspecie.png}
	\end{minipage}
	\begin{minipage}[c]{0.45\linewidth}
		\includegraphics[width=\linewidth]{14-CajaBrotesEspecie.png}
	\end{minipage}
	\caption{Cantidad de brotes por especie.}
	\label{fig:BrotesEspecie}
\end{figure}

En la figura \ref{fig:BrotesEspecie} se muestran diferentes representaciones del mismo conjunto de datos. En el gráfico de la izquierda se puede apreciar la cantidad total de brotes generados (y crecidos) por cada especie en el último año mientras que en el gráfico de la derecha se puede ver la distribución de la cantidad de brotes por cada especie.

Como en todas las especies salvo la especie Acacia no hay valores extremos, los promedios son una medida de resumen adecuada para trabajar. Más aún, si se combina el promedio de brotes por especie (gráfico derecho) con la cantidad de brotes por especie (gráfico izquierda), se puede estimar la cantidad de árboles que hay por especie muy rápidamente.

De forma complementaria a los gráficos anteriores se presenta en la figura \ref{fig:BrotesOrigen} un gráfico discriminado por origen. En dicho gráfico se puede apreciar que las especies de origen autóctono generan más brotes que las de origen exótico.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{15-CajaBrotesOrigen.png}
	\caption{Cantidad brotes por origen.}
	\label{fig:BrotesOrigen}
\end{figure}

\subsection{Análisis de datos relevantes}
En esta sección se analizarán los datos de forma cruzada. El análisis de estos datos es ligeramente más complejo que el análisis de los datos de la sección anterior. Aunque en la sección anterior también hubo sectores en donde los datos se complementaron, en esta sección se van a hacer análisis exclusivamente bivariados o multivariados.

\subsubsection{Árboles de origen autóctono}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{21-NAlturaDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{22-NAlturaInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{23-NDiametroInclinacion.png}
	\end{minipage}
	\caption{Gráficos de dispersión de árboles de origen autóctono.}
	\label{fig:ScatterNativo}
\end{figure}

\subsubsection{Árboles de origen exótico}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{24-EAlturaDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{25-EAlturaInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{26-EDiametroInclinacion.png}
	\end{minipage}
	\caption{Gráficos de dispersión de árboles de origen exótico.}
	\label{fig:ScatterExotico}
\end{figure}

\subsubsection{Árboles de origen autóctono y exóticos}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{27-NEAlturaDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{28-NEAlturaInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{29-NEDiametroInclinacion.png}
	\end{minipage}
	\caption{Gráficos de dispersión de árboles de origen nativo y exótico combinados.}
	\label{fig:ScatterNativoExotico}
\end{figure}

\section{Conclusiones}

\bibliographystyle{plain}
\begin{thebibliography}{9}
	\bibitem{KyotoProtocol} 
	United Nations,
	\emph{Kyoto Protocol to the United Nations Framework Convention on Climate Change}
	1997
	
	\bibitem{Polucion}
	Bertram W. Carnow, MD, Paul Meier,
	\emph{Air Pollution and Pulmonary Cancer}.
	2015.
\end{thebibliography}

\end{document}
\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{hyperref}

\usepackage{float}
\graphicspath{ {./Graficos/} }
\captionsetup{justification=centering,margin=2cm}

\usepackage[
top    = 1cm,
bottom = 1.5cm,
left   = 1.5cm,
right  = 1.5cm]
{geometry}

\title{Análisis del estado del arbolado urbano público. Provincia de Buenos Aires, año 2011.}
\author{Delfina Martín - Valentina Prato}
\date{March 2020}

\begin{document}

\maketitle

\null\vspace{\fill}
\begin{abstract}
	En este informe se analiza la distribución y las características principales del arbolado urbano público de la provincia de Buenos Aires, Argentina a partir de un análisis exhaustivo de los datos provistos por el Censo Forestal Urbano Publico realizado en el año 2011 en dos comunas al sur de la provincia de Buenos Aires.
\end{abstract}

\newpage

\section{Motivación del problema}
Existe sobrada evidencia \cite{Polucion} de que hay una relación estrecha entre la contaminación del aire y la probabilidad de desarrollar enfermedades de tipo respiratorias. De hecho, en países no desarrollados se estima que la tercera parte de las muertes están directamente relacionadas a causas ambientales.

Los árboles en general y el arbolado urbano público en particular cumplen un papel relevante en la lucha contra la contaminación del aire. El proceso transformador por el cual los árboles convierten gases tóxicos como lo es el CO2 en oxígeno, es el motor de la reducción del calentamiento global y de la emisión de gases de efecto invernadero \cite{KyotoProtocol}.

Es por esta razón que en este informe se pretende analizar el arbolado público urbano en base a la información provista por el Censo Forestal Urbano Público realizado en dos comunas del sur de Buenos Aires en el año 2011.

\section{Análisis de los datos provistos por el Censo Forestal Urbano Público, 2011}
\subsection{Descripción de las variables analizadas}
Se describen a continuación las variables registradas en el Censo Forestal Urbano Público a partir de las cuales se genera el análisis de datos.
\vspace{0.5cm}

\begin{tabular}{|c|l|}
	\hline
	\textbf{Identificador de la variable}&\textbf{Descripción de la variable}\\
	\hline
	Altura&Representa la altura del árbol medida en metros(m) truncada a la unidad\\
	\hline
	Diámetro&Representa el diámetro del árbol medido en centímetros(cm)\\
	\hline
	Inclinación&Representa el ángulo que forma el tronco del árbol respecto a una recta\\
	&perpendicular al suelo medido en grados($^\circ$)\\
	\hline
	Especie&Representa la especie a la que pertenece el árbol*\\
	\hline
	Origen&Representa la procedencia de la especie, dentro de las cuales se encuentran:\\
	&Nativo/Autóctono, Exótico y No determinado\\
	\hline
	Brotes&Representa el número de brotes jóvenes crecidos durante el último año\\
	\hline
\end{tabular}

\vspace*{4mm}
(*)Las especies incluídas en el Censo Forestal Urbano Público se pueden clasificar en las siguientes categorías: Eucalipto, Jacarandá, Palo borracho, Casuarina, Fresno, Ceibo, Ficus, Álamo y Acacia.

\subsection{Desarrollo del análisis de los datos}
\subsection{Análisis preliminar}
En esta sección se presentan diferentes resúmenes de datos generales mediante recursos relativamente sencillos. Este análisis resulta útil para tener un panorama general de los datos con los que se cuenta.

\subsubsection{Altura de los árboles}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.5]{1-BastonesAltura.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.3]{1-TablaAltura.png}
	\end{minipage}
	\caption{Gráfico de barra correspondiente a la altura de los árboles en la población estudiada con su tabla de frecuencias asociada.}
	\label{fig:Altura}
\end{figure}

Como se puede ver en la figura \ref{fig:Altura} y más detelladamente en la tabla de frecuencias asociada a su derecha, la altura de los árboles se concentra entre los 10m y los 16m aproximadamente.

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{2-CajaAltura.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{3-CajaAlturaOrigen.png}
	\end{minipage}
	\caption{Distribución de las alturas de los árboles y distribución de las alturas discriminada por origen.}
	\label{fig:CajaAltura}
\end{figure}

En la figura \ref{fig:CajaAltura} se aprecia con más detalle en qué rangos se concentran los datos del Censo Forestal Urbano Público. La primera cuarta parte de las alturas de los árboles ordenadas oscila entre los valores 1m y 9m mientras que la última cuarta parte varía entre 18m y 35m aproximadamente.\\
En el gráfico izquierdo de la figura \ref{fig:CajaAltura} están representadas la media aritmética y la mediana: 14.02m y 13m respectivamente. Por otra parte, en el gráfico de la derecha de la figura \ref{fig:CajaAltura} se puede observar que tanto la altura promedio como la mayor concentración de valores de los árboles de especies de origen exótico son mayores que estos mismos parámetros pero referidos a los árboles de especies de origen autóctono.


\subsubsection{Diámetro de los árboles}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.5]{4-BastonesDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.3]{4-TablaDiametro}
	\end{minipage}
	\caption{Gráfico de barra correspondiente a los diámetros de los árboles en la población estudiada con su tabla de frecuencias asociada.}
	\label{fig:Diametro}
\end{figure}

En la figura \ref{fig:Diametro} se puede observar que hay una gran cantidad de árboles con diámetros que varían entre 3cm y 63cm. De cualquier forma, es posible ver estos datos con más detalle en la figura \ref{fig:CajaDiametro}. El primer y tercer cuartil están en los valores 24cm y 53cm respectivamente, lo cual deja en evidencia muchos valores extremos. Además el diámetro de los árboles exóticos no varía mucho con respecto al diámetro de los árboles autóctonos, como se puede observar en el segundo gráfico de la figura \ref{fig:CajaDiametro}.

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{5-CajaDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{6-CajaDiametroOrigen.png}
	\end{minipage}
	\caption{Distribución de los diámetros de los árboles y distribución de los diámetros discriminados por origen.}
	\label{fig:CajaDiametro}
\end{figure}

\subsubsection{Inclinación de los árboles}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.5]{7-BastonesInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.4]{7-TablaInclinacion.png}
	\end{minipage}
	\caption{Gráfico de barra correspondiente a las inclinaciones de los árboles en la población estudiada con su tabla de frecuencias asociada.}
	\label{fig:Inclinacion}
\end{figure}

En la figura \ref{fig:Inclinacion} se puede ver como gran parte de los árboles en la población están inclinados entre 0$^{\circ}$ y 5$^{\circ}$. Por otra parte, en la figura \ref{fig:CajaInclinacion} se ve que el 75\% de los árboles analizados tienen inclinación menor o igual a 4$^{\circ}$ lo que se refleja en la mediana de 0$^{\circ}$. La existencia de valores extremos es lo que da lugar al promedio de inclinación de 3.12$^{\circ}$.

Al hacer un análisis en el cuál se diferencia el origen del árbol, como lo es el resumen del gráfico de la derecha de la figura \ref{fig:CajaInclinacion} se puede concluir que los árboles exóticos no se inclinan, salvo casos extremos.

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{8-CajaInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.4\linewidth}
		\includegraphics[scale=0.6]{9-CajaInclinacionOrigen.png}
	\end{minipage}
	\caption{Distribución de las inclinaciones de los árboles y distribución de las inclinaciones discriminadas por origen.}
	\label{fig:CajaInclinacion}
\end{figure}

\subsubsection{Especies de los árboles}

En la figura \ref{fig:Especies} se puede ver que la especie predominante en la población es el Palo Borracho, seguida por la especie Eucalipto dejando en último lugar a la especie Ficus.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{10-BarrasEspecie.png}
	\caption{Árboles por especie.}
	\label{fig:Especies}
\end{figure}

\subsubsection{Origen de los árboles}

Es importante destacar que en la sección anterior se hizo un análisis por especie que no contemplaba el origen. En la figura \ref{fig:Origen} se pueden ver las proporciones de árboles de origen exótico y autóctono, con lo cual la figura \ref{fig:Origen} complementa la figura \ref{fig:Especies}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{11-BarrasOrigen.png}
	\caption{Árboles por origen.}
	\label{fig:Origen}
\end{figure}

\subsubsection{Brotes de los árboles}

En el gráfico \ref{fig:Brotes} se muestra que en promedio cada árbol de la población permitió el crecimiento de 3.737 brotes. Como este valor no se ve afectado por valores extremos o remotos, el promedio resulta una medida de resumen significativa. El primer cuartil, el tercer cuartil y la mediana se encuentran en 3 brotes, 5 brotes y 4 brotes respectivamente.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{12-CajaBrotes.png}
	\caption{Cantidad de brotes por árbol.}
	\label{fig:Brotes}
\end{figure}

El análisis anterior no fue discriminado por especie ni por origen. A continuación se presentan algunos resúmenes gráficos en los que se categoriza la información según especie u origen lo cual permite realizar un análisis más exhaustivo con los datos disponibles.

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.45\linewidth}
		\includegraphics[width=\linewidth]{13-BarrasBrotesEspecie.png}
	\end{minipage}
	\begin{minipage}[c]{0.45\linewidth}
		\includegraphics[width=\linewidth]{14-CajaBrotesEspecie.png}
	\end{minipage}
	\caption{Cantidad de brotes por especie.}
	\label{fig:BrotesEspecie}
\end{figure}

En la figura \ref{fig:BrotesEspecie} se muestran diferentes representaciones del mismo conjunto de datos. En el gráfico de la izquierda se puede apreciar la cantidad total de brotes generados (y crecidos) por cada especie en el último año mientras que en el gráfico de la derecha se puede ver la distribución de la cantidad de brotes por cada especie.

Como en todas las especies salvo la especie Acacia no hay valores extremos, los promedios son una medida de resumen adecuada para trabajar. Más aún, si se combina el promedio de brotes por especie (gráfico derecho) con la cantidad de brotes por especie (gráfico izquierda), se puede estimar la cantidad de árboles que hay por especie muy rápidamente.

De forma complementaria a los gráficos anteriores se presenta en la figura \ref{fig:BrotesOrigen} un gráfico discriminado por origen. En dicho gráfico se puede apreciar que las especies de origen autóctono generan más brotes que las de origen exótico.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{15-CajaBrotesOrigen.png}
	\caption{Cantidad brotes por origen.}
	\label{fig:BrotesOrigen}
\end{figure}

\subsection{Análisis de datos relevantes}
En esta sección se analizarán los datos de forma cruzada. El análisis de estos datos es ligeramente más complejo que el análisis de los datos de la sección anterior. Aunque en la sección anterior también hubo sectores en donde los datos se complementaron, en esta sección se van a hacer análisis exclusivamente bivariados o multivariados.

En los siguientes diagramas de dispersión podemos ver los círculos, representando las observaciones, ubicados en base a dos variables, coloreados según su origen y con tamaño proporcional a la cantidad de brotes que se observaron. También podemos ver una linea de tendencia sobre el gráfico, marcando la posición general aproximada de los puntos.

\subsubsection{Árboles de origen autóctono}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{21-NAlturaDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{22-NAlturaInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{23-NDiametroInclinacion.png}
	\end{minipage}
	\caption{Gráficos de dispersión de árboles de origen autóctono.}
	\label{fig:ScatterNativo}
\end{figure}

En la figura \ref{fig:ScatterNativo} podemos ver las relaciones altura-diámetro, altura-inclinación y diámetro-inclinación de los arboles nativos observados, respectivamente.

Como la linea de ajuste del primer gráfico es ascendente, esto muestra que en general los arboles autóctonos de mayor altura tienen mayor diámetro, creciendo la línea aproximadamente de 20 a 70 cm de diámetro. Por el otro lado, esto no sucede con la inclinación, e incluso vemos que la relación de esta con el diámetro es mas inversa que con la altura, ya que la línea de ajuste en el segundo gráfico es casi constante mientras que en el tercero es descendiente.

\subsubsection{Árboles de origen exótico}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{24-EAlturaDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{25-EAlturaInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{26-EDiametroInclinacion.png}
	\end{minipage}
	\caption{Gráficos de dispersión de árboles de origen exótico.}
	\label{fig:ScatterExotico}
\end{figure}

En la figura \ref{fig:ScatterExotico} podemos ver una relación entre las variables para arboles exóticos similar a la de arboles nativos, especialmente en la línea ascendente de altura-diámetro, creciendo aproximadamente de 0 a 90 cm de diámetro. Sin embargo, en este caso las relaciones con la inclinación son diferentes, ya que, al contrario del caso anterior, su relación con la altura es inversa (la línea decrece) mientras que su relación con el diámetro es constante.

\subsubsection{Árboles de origen autóctono y exóticos}

\begin{figure}[H]
	\centering
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{27-NEAlturaDiametro.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{28-NEAlturaInclinacion.png}
	\end{minipage}
	\begin{minipage}[c]{0.3\linewidth}
		\includegraphics[width=\linewidth]{29-NEDiametroInclinacion.png}
	\end{minipage}
	\caption{Gráficos de dispersión de árboles de origen nativo y exótico combinados.}
	\label{fig:ScatterNativoExotico}
\end{figure}

En esta ultima figura \ref{fig:ScatterExotico} podemos ver la relación de variables para árboles tanto autóctonos como exóticos, y cuánto se diferencian. El primer gráfico es similar a los anteriores, con una linea de tendencia creciendo de aproximadamente 10 a 90 cm de diámetro, pero también se puede apreciar como algunos casos extremos de árboles exóticos alcanzan mucha mayor altura y diámetro que los autóctonos. Mientras tanto, en los gráficos de inclinación, podemos notar que ambos tienen líneas descendientes, marcando relaciones inversas con la inclinación para árboles en general, e incluso más inversa para la altura.

\section{Conclusiones}
En el informe presentado se pudieron analizar cada una de las variables registradas en el Censo Forestal Urbano Publico por separado, para ver que qué datos se contaba inicialmente. Dentro de esa misma sección se analizaron los datos suministrados de forma univariada y bivariada con el fin de aportar más riqueza al análisis.

En una sección posterior se hizo un análisis lorem ipsum
tendencias
porcentajes

Se espera que este informe contribuya a futuras investigaciones dentro del campo de la ingeniería forestal, así como también a campos afines.

\bibliographystyle{plain}
\begin{thebibliography}{9}
	\bibitem{KyotoProtocol} 
	United Nations,
	\emph{Kyoto Protocol to the United Nations Framework Convention on Climate Change}
	1997
	
	\bibitem{Polucion}
	Bertram W. Carnow, MD, Paul Meier,
	\emph{Air Pollution and Pulmonary Cancer}.
	2015.
\end{thebibliography}

\end{document}
 